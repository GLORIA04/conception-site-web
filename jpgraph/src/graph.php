<?
php
 // --- Inclure les bibliotheques
require 'jpgraph/src/jpgraph.php'; 
require 'jpgraph/src/jpgraph/jpgraph_pie.php';
 // --- Creer le conteneur graphique
$graph = new PieGraph(500, 200);
 // --- Creer le diagramme a secteurs
$donnees = array(23.3, 3.6, 12.8, 30, 30.3);
$legende = array('mer', 'lac', 'montagne', 'campagne', 'ville');
$camembert = new PiePlot($donnees);
$camembert->SetLegends($legende);
 // --- Ajouter le camembert au graphique
$graph->Add($camembert);
 // --- Choisir le format d'image
$graph->img->SetImgFormat("png");
 // --- Envoyer au navigateur
$graph->Stroke();
?>