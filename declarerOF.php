<?php session_start(); ?>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<link rel="stylesheet" href="header.css">
<link rel="stylesheet" href="menu.css">
<link rel="stylesheet" href="contenu.css">
<link rel="stylesheet" href="footer.css">
<link rel="stylesheet" href="fixe.css">
<title>Déclarer un objet retrouvé</title>
</head>
<body>
<?php include ("header.php"); ?>

<h1> AJOUTER UN OBJET RETROUVE </h1>

<?php include ("menuAdmi.php"); ?>

<article><br><br>

<fieldset>
<legend><b> Décriver l'objet retrouvé</b></legend>

<form action="insertOF.php" method="POST" enctype="multipart/form-data">

<p><label><b>Type d'objet:</b> <input type="text" name="typeF" placeholder="Ex: Iphone7" required></label></p>
<p><label><b>Lieu:</b> <input type="text" name="salleF" placeholder="Ex: Amphi Charliat" ></label></p>
<p><label><b>Retrouvé le:</b> <input type="date" name="dateF" placeholder="jj//mm/aaaa" required></label></p>
<p><label for="image"><b>Image:</b></label>
	<input type="file" id="image" name="image" ></p>
<p><label for="description" required><b>Description de l'objet:</b></label>
	<textarea type="text" id="description" name="descriptionF" rows="4" cols="40"></textarea></p>
<p><input type="submit" name="register" value="Ajouter"></p>
</form></fieldset>
</article>

<?php include ("footer.php");?>
</body>
</html>