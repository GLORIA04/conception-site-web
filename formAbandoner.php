
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<link rel="stylesheet" href="header.css">
<link rel="stylesheet" href="menu.css">
<link rel="stylesheet" href="contenu.css">
<link rel="stylesheet" href="footer.css">
<link rel="stylesheet" href="fixe.css">
<title> Gestionnaire</title>
</head>

<body>

<?php session_start(); ?>

<?php include ("header.php");?>

<?php include ('param.inc1.php');?>

<h1> ABANDONNER UN OBJET </h1>

<?php include ("menuGest.php");?>
<article>
<form action="traitementAban.php" method="POST">

<fieldset>
<p><b> Quel objet voulez-vous abandonner ?</b></p>
<p><select name="statut" required>
<option value="objectL" selected>Un objet perdu</option>
<option value="obojectF">Un objet retrouvé</option>
</select></p>

<p><label><b>Identifiant de l'objet:</b> <input type="text" name="id" placeholder="Ex: 0" required></label></p>
<p><input type="submit" name="register" value="Abandonner"></p>

</form>

</fieldset>

</article>
	
	<?php include ("footer.php");?>

	</body>
	</html>