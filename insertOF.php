<?php session_start(); ?>

<?php include ('param.inc1.php'); ?>

<?php
/*
$taille_max = 2000000;
$extensionValide = array('jpg','jpeg','gif','png');

if ($_FILES['image'] AND !empty($_FILES['image']['name'])) {
	$extensionUpload = strtolower(substr(strrchr($_FILES['image']['name'], '.'), 1)); //permet de récupérer l'extension de l'image
	
	if(in_array($extensionUpload, $extensionValide))
	{
		$path = "../image/oPerdu/.$_SESSION['id']."." .$extensionUpload;
		
		//Permet de transférer l'image dans la bdd (is_up.. pour mysql)
		$ans = move_uploaded_file($_FILES['image']['tmp_name'], $path);
		
		if(!$ans)
			echo'Problème de transfert';
		else
		{
			//Le fichier a bien été transféré
			$img_taille = $_FILES['image']['size'];
			if($img_taille>$taille_max)
				echo'Taille de l\'image trop grande !';
			else
				$updateImage = $bdd->prepare('UPDATE object SET image = :image WHERE id = :id');
					$updateImage->execute(array(
						'image' => $_SESSION['id'].".".$extensionUpload
						'id' => $_SESSION['id']
						));
		}

	}
}
else
	echo 'L\'image doit être du type : jpg, jpeg, gif, png !'
*/
?>

<?php

	if(isset($_SESSION['login'], $_SESSION['mp'])){
		
		if($_SESSION['statut']=='administrateur'){
		
	try{
					$bdd = new PDO('mysql:host=' . $host . ';dbname=' . $dbname . ';', $login,$password,array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION));
		
					$req = $bdd->prepare('INSERT INTO objectfind (typeF, salleF, dateF, descriptionF, statutF) VALUES(?, ?, ?, ?,?)');
					$test = $req->execute(array( htmlentities($_POST['typeF']), htmlentities($_POST['salleF']), htmlentities($_POST['dateF']), htmlentities($_POST['descriptionF']), 'Found'));
			if($test)
			{
				Echo 
				'<div>
				<p>Félicitation ! Vous venez d\'ajouter l\'objet retrouvé dont les caractéristiques sont: </p>
				<strong>Type : </strong>'. htmlentities($_POST['typeF']) . '<br>
								<strong>Lieu : </strong>'			. htmlentities($_POST['salleF'])   . '<br>
								<strong>Perdu le : </strong>'			. htmlentities($_POST['dateF'])   . '<br>
								<strong>Description : </strong>' 	. htmlentities($_POST['descriptionF']) . '
						</div>';
						
						header("refresh: 3;url=declarerOF.php");
			}
			else
			{
				echo'Une erreur s\'est produite ! Veuillez réessayer !';
				header("refresh: 3;url=declarerOF.php");
			}
	}
				catch(Exception $e)
				{
				    die('Erreur : '.$e->getMessage());
				}
	}
		 else{
			  echo'ATTENTION ! Vous \'êtes pas un administrateur !';
			  header("refresh: 3; url=declarerOL.php");
		 }
			
	}
	else
	{
		echo'Veuillez vous connecter !';
		header("refresh: 3; url=index.php");
	}
	
?>


