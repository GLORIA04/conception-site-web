
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<link rel="stylesheet" href="header.css">
<link rel="stylesheet" href="menu.css">
<link rel="stylesheet" href="contenu.css">
<link rel="stylesheet" href="footer.css">
<link rel="stylesheet" href="fixe.css">
<title> Liste objets retrouvés</title>



</head>

<body>

<style>
table, th, td{
	border: 1px solid black;
	border-collapse: collapse;
}
</style>

<?php session_start(); ?>

<?php include ("header.php");?>

<?php include ('param.inc1.php');?>

<h1> LISTE DES OBJETS RETROUVES</h1>

<?php include ("menuEleve.php");?>

	
	<article>
	
	<table style="width:100%; height=100%">
		<tr> 
	<th><b>Type</b></th>
	<th><b>Lieu</b></th>
	<th><b>Retrouvé le</b></th>
	<th><b>Identifiant</b></th><br>
	</tr>
		
		<?php
		if(isset($_SESSION['login'], $_SESSION['mp'])){
			
			if($_SESSION['statut']!='gestionnaire'){
		
		$tab = array();
		$bdd = new PDO('mysql:host=' . $host . ';dbname=' . $dbname . ';', $login,$password,array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION));
		//J'envoie la requête de récupérer certaines informations
		$reponse = $bdd->query('SELECT typeF, salleF, dateF, idF FROM objectfind');
		
		//mettre dans le tableau donnee chaque ligne de la requête tant qu'il y a tjrs des lignes
		while ($lignes = $reponse-> fetch(PDO::FETCH_ASSOC))
		{
			echo'<tr>';
			$tab['typeF'] = $lignes['typeF'];
			$tab['salleF'] = $lignes['salleF'];
			$tab['dateF'] = $lignes['dateF'];
			$tab['idF'] = $lignes['idF'];
			
			//Affichage du tableau
			if(empty($lignes['typeF']))
			{
				echo '
				<tr><td>Vide</td>';
			}
			else{
					echo '
					<td>'.$tab['typeF'].'</td>';
			}
			
			if(empty($lignes['salleF']))
			{
				echo '
				<td>Vide</td>';
			}
			else{
					echo '
					<td>'.$tab['salleF'].'</td>';
			}
			
			if(empty($lignes['dateF']))
			{
				echo '
				<td>Vide</td>';
			}
			else{
					echo '
					<td>'.$tab['dateF'].'</td>';
			}
			
			if(empty($lignes['idF']))
			{
				echo '
				<td>Vide</td></tr><br>';
			}
			else{
					echo '
					<td>'.$tab['idF'].'</td></tr><br>';
			}
			
		}
		
		}
		else
			echo'<em>ATTENTION ! Vous êtes un gestionnaire.</em>';
	}
		
	else
	{
		echo'Veuillez vous connecter !';
		header("refresh: 3; index.php");
	}
	
		?>
		</table>
		</article>
	
	<?php include ("footer.php");?>
	</table>
	</body>
	</html>