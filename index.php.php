<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<link rel="stylesheet" href="page_accueil.css">
<title>Page d'accueil</title>
</head>
<body> 


<header><div></div>
<div><a href="http://www.esigelec.fr"> <img src="visuel-logo-esigelec-34897.gif.jpg" alt="logo">
</a></div>
<div id="divb"><b>BIENVENUE !</b></div>
</header>


<h1 id="connx">CONNEXION</h1>

<nav><fieldset>
<legend><b>Entrez votre identifiant et votre mot de passe</b></legend>
<form action="join.php" method="POST">
<div><label>Identifiant: <br><input type="text" name="login" required></label></div>
<div><label>Mot de passe:<br><input type="password" name="mp" required></label></div>
<p id="p1"><input type="submit" value="SE CONNECTER" ></p>
<p id="p2"><input type="submit" value="Mot de passe oublié"></p>
</form></fieldset></nav>

<article>
<div id="divarticle"><span id="span1"><b>Etudiant</b></span> de l'Esigelec, avez-vous perdu un objet? Connectez-vous pour déclarer un objet perdu.
<br><br><span id="span1"><b>Administrateur</b></span>, 
gérer les objets perdus et retrouvés au sein de l'ESIGELEC !</div>
<p id="divinscrire"><a href="inscription.php"><input type="submit" value="S'INSCRIRE"></article></a></p>

<footer><b>Copyright © objetperdu.com</b></footer>
</body>
</html>