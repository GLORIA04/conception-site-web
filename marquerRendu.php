<?php session_start(); ?>

<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<link rel="stylesheet" href="header.css">
<link rel="stylesheet" href="menu.css">
<link rel="stylesheet" href="contenu.css">
<link rel="stylesheet" href="footer.css">
<link rel="stylesheet" href="fixe.css">
<title>Marquer comme rendu</title>
</head>
<body>


<?php include ("header.php"); ?>

<?php include ("menuAdmi.php"); ?>

<?php include('param.inc1.php')?>

<article>

			<?php 
			if(isset($_SESSION['login'], $_SESSION['mp'])){
				if($_SESSION['statut']=='administrateur'){
			
				
				try{
					$bdd = new PDO('mysql:host=' . $host . ';dbname=' . $dbname . ';', $login,$password,array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION));

				$reponse = $bdd->query('SELECT * FROM  objectfind WHERE idF = ' . htmlentities($_POST['idF']) );

					$donnees = $reponse->fetch();
					
						if($donnees['statutF'] =="Found"){
							
								echo 
								'<div>
								<p> Voulez-vous vraiment rendre cet objet ?</p>
								<strong>Type : </strong>'. $donnees['typeF'] . '<br>
								<strong>Lieu : </strong>'			. $donnees['salleF']  . '<br>
								<strong>Retrouvé le : </strong>'			. $donnees['dateF']  . '<br>
								<strong>Description : </strong>' 	. $donnees['descriptionF'] . '<br>
																		
								<strong style="color:red;">Statut : </strong>' . $donnees['statutF']   	  . '<br>
								<input type="submit" name="register" value="Rendre">
								</div>';
								
								if(isset($_POST['register'])){
									$reponse = $bdd->query('UPDATE objectfind SET statutF = "Rendu" WHERE idF = ' . $_POST['idF'] );			
					echo'L\'objet a été bien rendu !';
					header("refresh: 3;url=declarerOL.php");
								}

						}

						else{
							echo 
							'<em> ATTENTION ! Entrez un objet qui n\'est pas rendu ou qui existe ! </em><br/>';
							header("refresh: 3; url=formRendu.php");
						}
						
				}

			
				catch(Exception $e)
				{
				    die('Erreur : '.$e->getMessage());
				}
			}
			else
			{
				echo'ATTENTION ! Vous n\'êtes pas un administrateur !';
				header("refresh: 3; url=declarerOL.php");
			}
			}
			
			else
	{
		echo'VEUILLEZ VOUS CONNECTER EN TANT QU\'ADMINISTRATEUR !';
		header("refresh: 3; url=index.php");
	}

			?>
			
			</article>
			
			
<?php include ("footer.php");?>
</body>
</html>