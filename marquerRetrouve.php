<?php session_start(); ?>

<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<link rel="stylesheet" href="header.css">
<link rel="stylesheet" href="menu.css">
<link rel="stylesheet" href="contenu.css">
<link rel="stylesheet" href="footer.css">
<link rel="stylesheet" href="fixe.css">
<title>Marquer comme retrouvé</title>
</head>
<body>


<?php include ("header.php"); ?>

<?php include ("menuAdmi.php"); ?>

<?php include('param.inc1.php')?>

<article>

			<?php 
			if(isset($_SESSION['login'], $_SESSION['mp'])){
				if($_SESSION['statut']=='administrateur'){
			
				
				try{
					$bdd = new PDO('mysql:host=' . $host . ';dbname=' . $dbname . ';', $login,$password,array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION));

				$reponse = $bdd->query('SELECT * FROM  object WHERE id = ' . htmlentities($_POST['id']) );

					$donnees = $reponse->fetch();
					
						if($donnees['statut'] =="noFind"){
							
								echo 
								'<div>
								<p> Voulez-vous vraiment marquer cet objet comme retrouvé ?</p>
								<strong>Type : </strong>'. $donnees['type'] . '<br>
								<strong>Lieu : </strong>'			. $donnees['salle']  . '<br>
								<strong>Retrouvé le : </strong>'			. $donnees['date']  . '<br>
								<strong>Description : </strong>' 	. $donnees['description'] . '<br>
																		
								<strong style="color:red;">Statut : </strong>' . $donnees['statut']   	  . '<br>
								<input type="submit" name="register" value="Retrouver">
								</div>';
								
								if(isset($_POST['register'])){
									$reponse = $bdd->query('UPDATE object SET statut = "Retrouve" WHERE id = ' . $_POST['id'] );			
					echo'L\'objet a été bien retrouvé !';
					header("refresh: 5;url=declarerOL.php");
								}

						}

						else{
							echo 
							'<em> ATTENTION ! Rentrez un objet qui est non retrouvé ou qui existe !</em><br/>';
							header("refresh: 5; url=formRetrouve.php");
						}
						
				}

			
				catch(Exception $e)
				{
				    die('Erreur : '.$e->getMessage());
				}
			}
			else
			{
				echo'ATTENTION ! Vous n\'êtes pas un administrateur !';
				header("refresh: 3; url=declarerOL.php");
			}
			}
			
			else
	{
		echo'VEUILLEZ VOUS CONNECTER EN TANT QU\'ADMINISTRATEUR !';
		header("refresh: 5; url=index.php");
	}

			?>
			
			</article>
			
			
<?php include ("footer.php");?>
</body>
</html>