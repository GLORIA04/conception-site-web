<?php
 session_start();
 // --- Inclure les bibliotheques
require 'C:\Program Files (x86)\EasyPHP-Devserver-16.1\eds-www\jpgraph\src\jpgraph.php'; 
require 'C:\Program Files (x86)\EasyPHP-Devserver-16.1\eds-www\jpgraph\src\jpgraph_pie.php';
$valeur =23.3;

 // --- Creer le conteneur graphique
$graph = new PieGraph(588, 420);
 // --- Creer le diagramme a secteurs
$donnees = array($_SESSION['Retrouve'] ,  $_SESSION['noFind'] , $_SESSION['Abandonne'], $_SESSION['Found'] ,  $_SESSION['Rendu'] , $_SESSION['Abandonne']);
$legende = array('Objet perdu retrouvé', 'Objet perdu', 'Objet perdu abandonné', 'Objet trouvé', 'Objet rendu', 'Objet trouvé abandonné');
$camembert = new PiePlot($donnees);
$camembert -> SetLegends($legende);
 // --- Ajouter le camembert au graphique
$graph -> Add($camembert);
 // --- Choisir le format d'image
$graph -> img -> SetImgFormat("png");
 // --- Envoyer au navigateur
$graph->Stroke();
?>

