<?php session_start(); ?>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<link rel="stylesheet" href="header.css">
<link rel="stylesheet" href="menu.css">
<link rel="stylesheet" href="contenu.css">
<link rel="stylesheet" href="footer.css">
<link rel="stylesheet" href="fixe.css">
<title>Objet retrouvé</title>
</head>
<body>
<?php include ("header.php"); ?>

<h1> MARQUER UN OBJET COMME RETROUVE </h1>

<?php include ("menuAdmi.php"); ?>

<article><br><br>

<fieldset>
<legend><b> Quel objet avez-vous retrouvé ? </b></legend>

<form action="marquerRetrouve.php" method="POST">

<p><label><b>Identifiant de l'objet:</b> <input type="text" name="id" placeholder="Ex: 0" required></label></p>

<p><input type="submit" name="register" value="Cet objet"></p>
</form></fieldset>
</article>

<?php include ("footer.php");?>
</body>
</html>